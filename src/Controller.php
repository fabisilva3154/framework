<?php
	
	namespace Framework;

	class Controller 
	{

		protected $basePath    = '';
		protected $appPath     = '';
		protected $routes      = array();
		protected $middlewares = array();
		protected $RESTRoutes = array(
			array('GET' , 'show',   ''),
			array('GET' , 'create',  '/create'),
			array('POST', 'store',   '/create'),
			array('GET' , 'edit',    '/edit/[i:id]'),
			array('POST', 'update',  '/edit/[i:id]'),
			array('POST', 'changeOrden',    '/orden/[i:id]'),
			array('POST', 'delete',  '/delete/[i:id]'),
			array('POST', 'setNoted','/noted/[i:id]'),
		);
		static $twig;

		public function __construct($configuration)
		{
			if ( is_null($this::$twig) )
			{
				$this::$twig = $this::getTwigEnvironment($configuration);
			}
            $this->initialize();
		}
        
        public function initialize()
        {

        }

		function getTwigEnvironment($configuration)
        {
			$loader = new \Twig_Loader_Filesystem($configuration['templates']['dirs']);
			return new \Twig_Environment($loader, array());
		}

		public function injectRoutes($router, $basePath='')
		{
			foreach ($this->routes as $route)
			{
				$method = $route[0];
				$actionName = $route[1];
				$path = isset($route[2]) ? $route[2] : $actionName;
				$path = $basePath.$path;
				$action = new \Framework\Action($method, $path, $this, $actionName);
				$router->addRoute($action);
			}
		}

		public function injectRESTRoutes($router, $basePath='')
		{
			foreach ($this->RESTRoutes as $route)
			{
				$method = $route[0];
				$actionName = $route[1];
				$path = isset($route[2]) ? $route[2] : $actionName;
				$path = $basePath.$path;
				$action = new \Framework\Action($method, $path, $this, $actionName);
				$router->addRoute($action);
			}
		}

		public function execute($action)
		{
			$this->handleMiddlewares($action);
			$actionName = $action->getActionName();
			$this->$actionName($action);
		}

		public function handleMiddlewares($action)
		{
			foreach ($this->middlewares as $middleware) 
			{

				if ( isset($middleware[1]) && !in_array($action->getActionName(), $middleware[1]) )
				{
					continue;
				}

				$class = $middleware[0];
				$instance = new $class();
				$instance->setAppPath($this->appPath);
				$instance->handle($action->getInput());
			}
		}

		public function setFlashMessage($type, $text)
		{
			$_SESSION['message'] = array('type' => $type, 'text' => $text);
		}

		public function render($template, $variables=[])
		{
			$variables = array_merge($this->getDefaultViewParameters(), $variables);
			$variables = array_merge($this->getViewParameters(), $variables);
			echo $this::$twig->render($template, $variables);
		}

		public function renderTemplate($action)
		{
			$template = $action->getTemplateName();
			$this->render($template);
		}

		public function getDefaultViewParameters()
		{
			$parameters = array();
			if ( isset($_SESSION['message']) )
			{
				$parameters['message'] = $_SESSION['message'];
				unset($_SESSION['message']);
			}
			return $parameters;
		}

		public function getViewParameters()
		{
			return array();
		}

		public function json($variables)
		{
			echo json_encode($variables);
		}

		public function setBasePath($basePath)
		{
			$this->basePath = $basePath;
		}

        public function setAppPath($appPath)
        {
            $this->appPath = $appPath;
        }

		public function redirect($path)
		{
			header("Location:{$this->appPath}{$this->basePath}$path");
			exit();
		}
		
	}


?> 
