<?php
	
	namespace Framework;

	class Middleware
	{
	
		protected $basePath = '';
		protected $appPath  = '';

		public function handle($input)
		{
			
		}

        public function setAppPath($appPath)
        {
            $this->appPath = $appPath;
        }

		public function setBasePath($basePath)
		{
			$this->basePath = $basePath;
		}

		public function redirect($path)
		{
			header("Location:{$this->appPath}{$this->basePath}$path");
		}

	}

?>
