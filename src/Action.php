<?php  
    
    namespace Framework;

    class Action
    {

        protected $method;
        protected $path;
        protected $actionName;
        protected $controller;
        protected $templateName;
        protected $input = array();
        protected $params = array();

        public function __construct($method, $path, $controller, $actionName, $templateName=null)
        {
            $this->method = $method;
            $this->path = $path;
            $this->controller = $controller;
            $this->actionName = $actionName;
            $this->templateName = $templateName;
        }

        public function getMethod()
        {
            return $this->method;
        }

        public function setMethod($method)
        {
            $this->method = $method;
            return $this;
        }

        public function getPath()
        {
            return $this->path;
        }

        public function setPath($path)
        {
            $this->path = $path;
            return $this;
        }

        public function getActionName()
        {
            return $this->actionName;
        }

        public function setActionName($actionName)
        {
            $this->actionName = $actionName;
            return $this;
        }

        public function getController()
        {
            return $this->controller;
        }

        public function setController($controller)
        {
            $this->controller = $controller;
            return $this;
        }

        public function getInput()
        {
            return $this->input;
        }

        public function setInput($input)
        {
            $this->input = $input;
            return $this;
        }

        public function getParams()
        {
            return $this->params;
        }

        public function setParams($params)
        {
            $this->params = $params;
            return $this;
        }

        public function getTemplateName()
        {
            return $this->templateName;
        }

        public function setTemplateName($templateName)
        {
            $this->templateName = $templateName;
            return $this;
        }
    }

?>