<?php  

	namespace Framework;

	class Router
	{

		private $basePath = '';
		private $altorouter;
		private $controller;


		public function __construct($configuration=array())
		{
			session_start();
			$this->altorouter = new \AltoRouter();
			$this->controller = new \Framework\Controller($configuration);
			$this->configuration = $configuration;
		}

		public function addController($class, $options=array())
		{
			$instance = new $class($this->configuration);
			$options = array_merge(array('basePath' => '/', 'rest' => false), $options);
			$instance->setAppPath($this->basePath);
			$instance->setBasePath($options['basePath']);
			if ( $options['rest'] )
			{
				$instance->injectRESTRoutes($this, $options['basePath']);
			}
			$instance->injectRoutes($this, $options['basePath']);
			return $class;
		}

		public function setBasePath($basePath)
		{
			$this->basePath = $basePath;
			$this->altorouter->setBasePath($basePath);
		}

		public function addRoute($action)
		{
			$this->altorouter->map($action->getMethod(), $action->getPath(), $action);
		}

		public function addView($path, $templateName)
		{
			$action = new \Framework\Action('GET', $path, $this->controller, 'renderTemplate', $templateName);
			$this->addRoute($action);
		}

		public function resolve($action, $params)
		{
			$action->setInput($this->getHTTPInput($action));
			$action->setParams($params);
			$controller = $action->getController();
			$controller->execute($action);
		}

		public function getHTTPInput($action)
		{
			$input = $_GET;
			if ( $action->getMethod() == 'PUT' ){
				$input = array_merge($input, $this->getPutInputs());
			} else {
				$input = array_merge($input, $_POST);
			}
			if ( count($_FILES) )
			{
				$input['files'] = $this->getFileList();
			}
            return $input;
		}

		public function getPutInputs()
		{
			$putData = file_get_contents("php://input");
			$inputs = explode('&', $putData);
			$result = array();
			foreach ( $inputs as $inputString )
			{
				$inputArray = explode('=', $inputString);
				$result[$inputArray[0]] = count($inputArray) > 1 ? $inputArray[1] : '';
			}
			return $result;
		}

		private function getFileList()
		{
			$files = array();
			$fileAttributes = array('tmp_name', 'name', 'type', 'size', 'error');
			foreach ($_FILES as $key => $value) 
			{
				if ( !in_array($key, $fileAttributes) )
				{
					if ( is_array($value['name']) )
					{
						$files[$key] = array();
						foreach ($value['name'] as $index => $field)
						{
							$file = array();
							foreach ($fileAttributes as $attribute)
							{
								$file[$attribute] = $value[$attribute][$index];
							}
							array_push($files[$key], $file);
						}
					} 
					else if ( !$value['error'] )
					{
						$files[$key] = array($value);
					}
				}
			}
			return $files;
		}

		public function listen()
		{
			$match = $this->altorouter->match();
			if ( $match )
			{
				$this->resolve($match['target'], $match['params']);
			} 
			else
			{
				$this->render404();
			}
		}

		public function render404()
		{
			header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
			echo 'La ruta no engancha';
		}
	}
	

?>
